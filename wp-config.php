<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'social_v3' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'qkr7vadd )v$re+~e1fqkx6 O8>],q$jNX+e3?(7rp-1#x*swD4&}j:9;X[.y4yZ' );
define( 'SECURE_AUTH_KEY',  '.BsepD_vJeX~=wg.*gE>P!SrXss[@eBz:@mCdl{31 4rF TZ$b`&<[QizIr,.1,u' );
define( 'LOGGED_IN_KEY',    '~%>0m<&sZ6nQ`VG*-S3Y)LY|3E=*N4~!qPYrQN{m%k0#A=X_5MH>~6t9Q5bZjVn<' );
define( 'NONCE_KEY',        'RjsJX:cI Rv{?2bq/Eb2e zriuNo!oLk:A_-Z>IY}5k;0|y3$PZP=1$TO&dqhAe3' );
define( 'AUTH_SALT',        's;1{?nrd+a~n<6kH/agO4-=DBAG=:vvm9)XstzJ^7^]w#cR(xFiuMEt7~g;|0GoQ' );
define( 'SECURE_AUTH_SALT', 'v9eEb5IVO[i+F+Ekj=<R_K|p&r!Ie4z{Ht6F9e.o6M_mE>+p1q<RB`X4R:0l=dd9' );
define( 'LOGGED_IN_SALT',   '3)8n EwZ31<!FqkY`>B7Oz1HP),$NN>HwAG;|r5WP|^NGWhD,{H1-`Q[+*Y>E3E ' );
define( 'NONCE_SALT',       '6ea~k:j?,5AK.M.lIR:#Lj;X_|a|BK2#.aMkw,j_.m;~+=|`UGqck>zM4!0RUN<]' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
 define( 'WP_DEBUG', false );
// define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
