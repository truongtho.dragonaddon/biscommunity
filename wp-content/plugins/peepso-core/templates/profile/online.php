<!-- Online status -->
<span aria-label="<?php echo sprintf(__('%s- is currently online','peepso-core'), $PeepSoUser->get_fullname()); ?>" class="ps-online ps-online--md ps-tip ps-tip--inline<?php echo isset($class) ? " $class" : '';?>"></span>
