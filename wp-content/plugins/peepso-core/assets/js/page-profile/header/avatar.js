import $ from 'jquery';
import { hooks } from 'peepso';
import { profile as profileData } from 'peepsodata';
import avatarDialog from './avatar-dialog';

const PROFILE_ID = +profileData.id;

$(function() {
	let $container = $('.ps-js-focus--profile .ps-js-avatar');
	if (!$container.length) {
		return;
	}

	let $image = $container.find('.ps-js-avatar-image');
	hooks.addAction('profile_avatar_updated', 'page_profile', (id, imageUrl) => {
		if (+id === PROFILE_ID) {
			$image.attr('src', imageUrl);
		}
	});

	let $button = $container.find('.ps-js-avatar-button');
	$button.on('click', e => {
		e.preventDefault();

		avatarDialog().show();
	});
});
