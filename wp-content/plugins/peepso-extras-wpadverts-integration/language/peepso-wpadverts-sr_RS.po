msgid ""
msgstr ""
"Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"Project-Id-Version: peepso\n"
"POT-Creation-Date: 2020-04-03 08:25+0700\n"
"PO-Revision-Date: 2020-04-03 01:46\n"
"Last-Translator: \n"
"Language-Team: Serbian (Cyrillic)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.12\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: peepso-wpadverts.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Crowdin-Project: peepso\n"
"X-Crowdin-Language: sr\n"
"X-Crowdin-File: peepso-wpadverts.pot\n"
"Language: sr_SP\n"

#: classes/admin/configsectionwpadverts.php:21
msgid "Classifieds menu will be available on User Profiles."
msgstr "Мени са малим огласима ће бити доступан на корисничким профилима."

#: classes/admin/configsectionwpadverts.php:24
msgid "Enable User Classifieds"
msgstr "Омогући мале огласе корисника"

#: classes/admin/configsectionwpadverts.php:30
msgid "Use default WPAdverts template."
msgstr "Употреби основни WPAdverts шаблон."

#: classes/admin/configsectionwpadverts.php:33
msgid "Disable Template Overrides"
msgstr "Онемогући преправке шаблона"

#: classes/admin/configsectionwpadverts.php:39
msgid "Classifieds need to be accepted by Admin before being published"
msgstr "Maли oгласи морају да буду прихваћени од стране Админа пре објаве"

#: classes/admin/configsectionwpadverts.php:42
msgid "Enable WPAdverts Moderation"
msgstr "Омогући модерацију WPAdverts"

#: classes/admin/configsectionwpadverts.php:47
msgid "List"
msgstr "Листа"

#: classes/admin/configsectionwpadverts.php:48
msgid "Grid"
msgstr "Мрежа"

#: classes/admin/configsectionwpadverts.php:54
msgid "Display Ads as"
msgstr "Прикажи огласе као"

#: classes/admin/configsectionwpadverts.php:62
msgid "Allow guest access to Classifieds"
msgstr "Дозволи приступ гостима у малим огласима"

#: classes/admin/configsectionwpadverts.php:69
msgid "General"
msgstr "Опште"

#: classes/admin/configsectionwpadverts.php:77
msgid "Turn on to post classifieds to stream when user create new classifieds"
msgstr "Укључи ради постовања малих огласа на проток активности када корисник креира нове мале огласе"

#: classes/admin/configsectionwpadverts.php:80
msgid "Post to Stream"
msgstr "Постуј на проток активности"

#: classes/admin/configsectionwpadverts.php:98
msgid "Default post privacy"
msgstr "Основна приватност поста"

#: classes/admin/configsectionwpadverts.php:105
msgid "Activity Stream Integration"
msgstr "Интеграција протока активности"

#: classes/admin/configsectionwpadverts.php:115
#, php-format
msgid "This feature requires the %s plugin."
msgstr "Ова одлика захтева %s додатак."

#: classes/admin/configsectionwpadverts.php:121
msgid "Display Chat button"
msgstr "Прикажи дугме за Ћаскање"

#: classes/admin/configsectionwpadverts.php:128
msgid "Chat Integration"
msgstr "Интеграција Ћаскања"

#: classes/admin/configsectionwpadverts.php:136
#, php-format
msgid "This section is for <b>advanced users only</b>.<br/>When modifying the profile slug, please do NOT use any keyword already in use (eg %s)."
msgstr "Овај одељак је <b>само за напредне кориснике</b>.<br/>Када модификујете slug назив профила, молимо да НЕ користите кључне речи који су већ у употреби (нпр %s)."

#: classes/admin/configsectionwpadverts.php:140
#: classes/admin/configsectionwpadverts.php:148
#: classes/admin/configsectionwpadverts.php:156
msgid "Leave empty for default value"
msgstr "Остави празно за основну вредност"

#: classes/admin/configsectionwpadverts.php:143
msgid "Menu item label (main)"
msgstr "Налепница за ставку на менију (главно)"

#: classes/admin/configsectionwpadverts.php:151
msgid "Menu item label (profiles)"
msgstr "Налепница за ставку на менију (профили)"

#: classes/admin/configsectionwpadverts.php:159
msgid "Profile slug"
msgstr "Ѕlug назив профила"

#: classes/admin/configsectionwpadverts.php:168
msgid "Advanced"
msgstr "Напредно"

#: classes/api/classifiedsajax.php:49
msgid "You have no classifieds yet"
msgstr "Још увек немате мале огласе"

#: classes/api/classifiedsajax.php:49
#, php-format
msgid "%s has no classifieds yet"
msgstr "%s још увек нема мале огласе"

#: classes/api/classifiedsajax.php:52
msgid "No classifieds found"
msgstr "Нису пронађени мали огласи"

#: classes/shortcodes/wpadvertsshortcode.php:25
msgid "Displays the WpAdverts listing."
msgstr "Приказује WpAdverts листинг."

#: classes/shortcodes/wpadvertsshortcode.php:29
msgctxt "Page listing"
msgid "PeepSo"
msgstr "PeepSo"

#: classes/shortcodes/wpadvertsshortcode.php:29
msgid "WP Adverts"
msgstr "WP Adverts"

#: classes/shortcodes/wpadvertsshortcode.php:65 install/activate.php:27
#: peepso-wpadverts.php:663 peepso-wpadverts.php:685
#: templates/wpadverts/wpadverts-category.php:20
#: templates/wpadverts/wpadverts.php:17 templates/wpadverts/wpadverts.php:21
msgid "Classifieds"
msgstr "Мали огласи"

#: peepso-wpadverts.php:165 peepso-wpadverts.php:574
#: templates/wpadverts/wpadverts-item.php:41
msgid "Send Message"
msgstr "Пошаљи поруку"

#: peepso-wpadverts.php:258
#, php-format
msgid "PeepSo %s requires the WpAdverts plugin."
msgstr "PeepSo %s захтева WpAdverts додатак."

#: peepso-wpadverts.php:291
#, php-format
msgid "The %s plugin requires the PeepSo plugin to be installed and activated."
msgstr "Додатак %s захтева да PeepSo додатак буде инсталиран и активиран."

#: peepso-wpadverts.php:293
msgid "Get it now!"
msgstr "Преузми одмах!"

#: peepso-wpadverts.php:342 peepso-wpadverts.php:577
#: templates/overrides/manage.php:62 templates/overrides140/manage.php:62
#: templates/wpadverts/wpadverts-item.php:48
msgid "More"
msgstr "Више"

#: peepso-wpadverts.php:343
msgid "Less"
msgstr "Мање"

#: peepso-wpadverts.php:344
msgid "member"
msgstr "члан"

#: peepso-wpadverts.php:345
msgid "members"
msgstr "чланови"

#: peepso-wpadverts.php:507
msgid "WPAdverts"
msgstr "WPAdverts"

#: peepso-wpadverts.php:510
msgid "PeepSo - WPAdverts Integration"
msgstr "PeepSo - WPAdverts интеграција"

#: peepso-wpadverts.php:569 templates/wpadverts/wpadverts-item.php:30
msgid "expires"
msgstr "истиче"

#: peepso-wpadverts.php:601
msgid " posted a new ad"
msgstr " је постовао/ла нови оглас"

#: templates/overrides/add-preview.php:9
#: templates/overrides140/add-preview.php:10
msgid "Edit Listing"
msgstr "Уреди листинг"

#: templates/overrides/add-preview.php:16
#: templates/overrides140/add-preview.php:18
msgid "Publish Listing"
msgstr "Објави листинг"

#: templates/overrides/add-save.php:5 templates/overrides140/add-save.php:5
msgid "Your ad has been put into moderation, please wait for admin to approve it."
msgstr "Ваш оглас је подвргнут модерацији, молимо сачекајте да га админ одобри."

#: templates/overrides/add-save.php:7 templates/overrides140/add-save.php:7
#, php-format
msgid "Your ad has been published. You can view it here \"<a href=\"%1$s\">%2$s</a>\"."
msgstr "Ваш оглас је објављен. Можете га видети овде \"<a href=\"%1$s\">%2$s</a>\"."

#: templates/overrides/add.php:68 templates/overrides/manage.php:59
#: templates/overrides140/add.php:68 templates/overrides140/manage.php:59
#: templates/wpadverts/wpadverts-item.php:86
msgid "Cancel"
msgstr "Откажи"

#: templates/overrides/add.php:71 templates/overrides/manage-edit.php:49
#: templates/overrides140/add.php:71 templates/overrides140/manage-edit.php:49
msgid "Preview"
msgstr "Преглед"

#: templates/overrides/categories-top.php:24
#: templates/overrides140/categories-top.php:24
msgid "No categories found."
msgstr "Нису пронађене било какве категорије."

#: templates/overrides/manage-edit.php:46
#: templates/overrides140/manage-edit.php:46
msgid "Go back"
msgstr "Иди назад"

#: templates/overrides/manage-edit.php:50
#: templates/overrides140/manage-edit.php:50
msgid "Update"
msgstr "Ажурирај"

#: templates/overrides/manage.php:30 templates/overrides140/manage.php:30
msgid "Inactive — This Ad is in moderation."
msgstr "Неактивно - Овај оглас је подвргнут модерацији."

#: templates/overrides/manage.php:34 templates/overrides140/manage.php:34
msgid "Inactive — This Ad expired."
msgstr "Неактивно - Овом огласу је истекла важност."

#: templates/overrides/manage.php:44 templates/overrides140/manage.php:44
#, php-format
msgid "Expires: %s"
msgstr "Важност истиче: %s"

#: templates/overrides/manage.php:48 templates/overrides140/manage.php:48
msgid "View"
msgstr "Погледај"

#: templates/overrides/manage.php:49 templates/overrides140/manage.php:49
#: templates/wpadverts/wpadverts-item.php:75
msgid "Edit"
msgstr "Уреди"

#: templates/overrides/manage.php:50 templates/overrides/manage.php:51
#: templates/overrides140/manage.php:50 templates/overrides140/manage.php:51
#: templates/wpadverts/wpadverts-item.php:55
#: templates/wpadverts/wpadverts-item.php:60
#: templates/wpadverts/wpadverts-item.php:65
#: templates/wpadverts/wpadverts-item.php:70
msgid "Delete"
msgstr "Обриши"

#: templates/overrides/manage.php:56 templates/overrides140/manage.php:56
#: templates/wpadverts/wpadverts-item.php:84
msgid "Are you sure?"
msgstr "Да ли сте сигурни?"

#: templates/overrides/manage.php:58 templates/overrides140/manage.php:58
#: templates/wpadverts/wpadverts-item.php:85
msgid "Yes"
msgstr "Да"

#: templates/overrides/manage.php:76 templates/overrides140/manage.php:76
msgid "You do not have any Ads posted yet."
msgstr "Још увек немате постованих огласа."

#: templates/overrides/single.php:19 templates/overrides140/single.php:21
#, php-format
msgid "by <strong>%s</strong>"
msgstr "од стране <strong>%s</strong>"

#: templates/overrides/single.php:20 templates/overrides140/single.php:22
#, php-format
msgid "Published: %1$s (%2$s ago)"
msgstr "Објављено: %1$s (пре %2$s)"

#: templates/overrides/single.php:37 templates/overrides140/single.php:43
#: templates/wpadverts/wpadverts.php:52
msgid "Category"
msgstr "Категорија"

#: templates/overrides/single.php:52 templates/overrides140/single.php:58
#: templates/wpadverts/wpadverts.php:45
msgid "Location"
msgstr "Локација"

#: templates/wpadverts/profile-wpadverts.php:12
#: templates/wpadverts/wpadverts-category.php:12
#: templates/wpadverts/wpadverts.php:12
msgid "Create"
msgstr "Креирај"

#: templates/wpadverts/wpadverts-category.php:16
#: templates/wpadverts/wpadverts-category.php:21
#: templates/wpadverts/wpadverts.php:22
msgid "Classifieds Categories"
msgstr "Категорије малих огласа"

#: templates/wpadverts/wpadverts-item.php:30
msgid "expired"
msgstr "истекла важност"

#: templates/wpadverts/wpadverts.php:28 templates/wpadverts/wpadverts.php:46
msgid "Start typing to search..."
msgstr "Почните да куцате за претрагу..."

#: templates/wpadverts/wpadverts.php:54
msgid "All categories"
msgstr "Све категорије"

#. Plugin Name of the plugin/theme
msgid "PeepSo Monetization: WPAdverts"
msgstr "PeepSo монетизација: WPAdverts"

#. Plugin URI of the plugin/theme
#. Author URI of the plugin/theme
msgid "https://peepso.com"
msgstr "https://peepso.com"

#. Description of the plugin/theme
msgid "<strong>Requires the WpAdverts plugin</strong>. Integrate WPAdverts  with PeepSo"
msgstr "<strong>Захтева WpAdverts додатак</strong>. Интегришите WPAdverts  са PeepSo"

#. Author of the plugin/theme
msgid "PeepSo"
msgstr "PeepSo"

