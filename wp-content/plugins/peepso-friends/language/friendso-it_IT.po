msgid ""
msgstr ""
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Project-Id-Version: peepso\n"
"POT-Creation-Date: 2020-04-03 08:19+0700\n"
"PO-Revision-Date: 2020-04-03 01:46\n"
"Last-Translator: \n"
"Language-Team: Italian\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.12\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: peepsofriends.php\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Crowdin-Project: peepso\n"
"X-Crowdin-Language: it\n"
"X-Crowdin-File: friendso.pot\n"
"Language: it_IT\n"

#: classes/friendsajax.php:60 classes/friendsajax.php:95
#: classes/friendsajax.php:154 classes/friendsajax.php:211
msgid "Notice"
msgstr "Avviso"

#: classes/friendsajax.php:61
msgid "Friend Request Sent"
msgstr "Richiesta di amicizia inviata"

#: classes/friendsajax.php:96
msgid "Friend Request Cancelled"
msgstr "Richiesta di amicizia annullata"

#: classes/friendsajax.php:155 classes/friendsconfig.php:81
msgid "Friend Request Accepted"
msgstr "Richiesta di amicizia accettata"

#: classes/friendsajax.php:212
msgid "Friend Removed"
msgstr "Amico rimosso"

#: classes/friendsajax.php:233 templates/friends/pending.php:33
msgid "You currently have no friend requests"
msgstr "Attualmente non hai richieste di amicizia"

#: classes/friendsajax.php:293
msgid "You have no friends yet"
msgstr "Non hai ancora amici"

#: classes/friendsajax.php:293
#, php-format
msgid "%s has no friends yet"
msgstr "%s non ha ancora amici"

#: classes/friendsajax.php:338 classes/widgets/widgetmutualfriends.php:107
msgid "Mutual Friends"
msgstr "Amici in comune"

#: classes/friendsconfig.php:40
msgid "Members can send messages to"
msgstr "I membri possono inviare messaggi a"

#: classes/friendsconfig.php:60
msgid "Friends Settings"
msgstr "Impostazioni Amici"

#: classes/friendsconfig.php:76
msgid "Friend Request Received"
msgstr "Richiesta di amicizia ricevuta"

#: classes/friendsconfig.php:77
msgid "This will be sent to a user when a friend request is received."
msgstr "Questo verrà inviato ad un utente quando riceve una richiesta di amicizia."

#: classes/friendsconfig.php:82
msgid "This will be sent to a user when a friend request is accepted."
msgstr "Questo verrà inviato ad un utente quando viene accettata una richiesta di amicizia."

#: classes/friendspostswidget.php:16
msgid "Friends Posts"
msgstr "Post degli amici"

#: classes/friendspostswidget.php:17
msgid "This will select up to 5 posts, displaying the most recent post by each of up to five users that are friends."
msgstr "Questo selezionerà fino a 5 messaggi, mostrando il post più recente di ciascuno fino a cinque utenti che sono amici."

#: classes/friendsrequests.php:58
msgid "Friend request already sent"
msgstr "Richiesta di amicizia già inviata"

#: classes/friendsrequests.php:72 classes/friendsrequests.php:104
#: classes/friendsrequests.php:142
msgid "You do not have enough permissions."
msgstr "Non si dispone di sufficienti autorizzazioni."

#: classes/friendsrequests.php:282 classes/friendsshortcode.php:135
#: peepsofriends.php:795
msgid " mutual friend"
msgid_plural " mutual friends"
msgstr[0] "amico in comune"
msgstr[1] "amici in comune"

#: classes/friendsrequests.php:469
#, php-format
msgid "New Friend Request from %s"
msgstr "Nuova richiesta di amicizia da %s"

#: classes/friendsrequests.php:489
#, php-format
msgid "%s accepted your friend request"
msgstr "%s ha accettato la tua richiesta di amicizia"

#: classes/friendsrequests.php:499
msgid "accepted your friend request"
msgstr "ha accettato la tua richiesta di amicizia"

#: classes/friendsrequests.php:520
msgid "Friend Request Denied"
msgstr "Richiesta di amicizia negata"

#: classes/friendsrequests.php:530 classes/friendsrequests.php:540
msgid "Friend Request Ignored"
msgstr "Richiesta di amicizia ignorata"

#: classes/widgets/widgetfriends.php:17
msgid "PeepSo Friends"
msgstr "PeepSo Amici"

#: classes/widgets/widgetfriends.php:18
msgid "PeepSo Friends Widget"
msgstr "Widget PeepSo Amici"

#: classes/widgets/widgetfriends.php:125
msgid "My Friends"
msgstr "I miei amici"

#: classes/widgets/widgetfriendsbirthday.php:14
msgid "PeepSo Friends Birthday"
msgstr "PeepSo Compleanni Amici"

#: classes/widgets/widgetfriendsbirthday.php:15
msgid "PeepSo Friends Birthday Widget"
msgstr "Widget PeepSo Compleanni Amici"

#: classes/widgets/widgetfriendsbirthday.php:138
msgid "Upcoming Friends Birthdays"
msgstr "Prossimi compleanni degli amici"

#: classes/widgets/widgetfriendsbirthday.php:151
msgid "General Settings"
msgstr "Impostazioni Generali"

#: classes/widgets/widgetfriendsbirthday.php:154
msgid "Title"
msgstr "Titolo"

#: classes/widgets/widgetfriendsbirthday.php:164
msgid "Hide if empty"
msgstr "Nascondi se vuoto"

#: classes/widgets/widgetfriendsbirthday.php:167
msgid "Upcoming Birthdays"
msgstr "Prossimi compleanni"

#: classes/widgets/widgetfriendsbirthday.php:173
msgid "Show upcoming birthdays"
msgstr "Visualizza prossimi compleanni"

#: classes/widgets/widgetfriendsbirthday.php:178
msgid "How many days ahead?"
msgstr "Quanti giorni a venire?"

#: classes/widgets/widgetfriendsbirthday.php:193
msgid "Max number of upcoming birthdays to show:"
msgstr "Numero massimo di compleanni imminenti da mostrare:"

#: classes/widgets/widgetmutualfriends.php:15
msgid "PeepSo Mutual Friends"
msgstr "PeepSo Amici in comune"

#: classes/widgets/widgetmutualfriends.php:16
msgid "PeepSo Mutual Friends Widget"
msgstr "Widget Peepso Amici in comune"

#: install/activate.php:80 peepsofriends.php:726 peepsofriends.php:815
#: peepsofriends.php:843 templates/friends/submenu.php:6
msgid "Friends"
msgstr "Amici"

#: install/activate.php:85 peepsofriends.php:334 peepsofriends.php:335
msgid "Friend Requests"
msgstr "Richieste di amicizia"

#: peepsofriends.php:269
#, php-format
msgid "The %s plugin requires the PeepSo plugin to be installed and activated."
msgstr "Il plugin %s richiede che il plugin PeepSo sia installato e attivato."

#: peepsofriends.php:271
msgid "Get it now!"
msgstr "Scaricalo ora!"

#: peepsofriends.php:408
msgid "Friends Only"
msgstr "Solo amici"

#: peepsofriends.php:484 peepsofriends.php:486
msgid "Remove Friend"
msgstr "Rimuovi amico"

#: peepsofriends.php:485
msgid "Are you sure?"
msgstr "Sei sicuro?"

#: peepsofriends.php:487
msgid "Cancel"
msgstr "Annulla"

#: peepsofriends.php:507 peepsofriends.php:516
msgid "Friend"
msgstr "Amico"

#: peepsofriends.php:512
msgid "Remove friend"
msgstr "Rimuovi amico"

#: peepsofriends.php:515
msgid "Unfriend"
msgstr "Rimuovi dagli amici"

#: peepsofriends.php:527 peepsofriends.php:532
msgid "Cancel Friend Request"
msgstr "Annulla richiesta di amicizia"

#: peepsofriends.php:542
msgid "Accept"
msgstr "Accetta"

#: peepsofriends.php:547
msgid "Accept Request"
msgstr "Accetta la richiesta"

#: peepsofriends.php:553
msgid "Reject"
msgstr "Rifiuta"

#: peepsofriends.php:558
msgid "Reject Request"
msgstr "Rifiuta la richiesta"

#: peepsofriends.php:568
msgid "Add as Friend"
msgstr "Aggiungi come amico"

#: peepsofriends.php:573
msgid "Add as your friend"
msgstr "Aggiungi come amico"

#: peepsofriends.php:588
msgid "Not following"
msgstr "Non seguire più"

#: peepsofriends.php:589
msgid "Click to follow this user"
msgstr "Clicca per seguire questo utente"

#: peepsofriends.php:593
msgid "Start following"
msgstr "Segui"

#: peepsofriends.php:597
msgid "Following"
msgstr "Segui già"

#: peepsofriends.php:598
msgid "Click to unfollow this user"
msgstr "Clicca per non seguire più questo utente"

#: peepsofriends.php:601
msgid "Stop following"
msgstr "Non seguire più"

#: peepsofriends.php:729
msgid "Someone sent me a Friend Request"
msgstr "Qualcuno mi ha inviato una richiesta di amicizia"

#: templates/friends/notification-popover-item.php:28
msgid "Ignore"
msgstr "Ignora"

#: templates/friends/notification-popover-item.php:31
msgid "Approve"
msgstr "Approva"

#: templates/friends/submenu.php:9
msgid "Friend requests"
msgstr "Richieste di amicizia"

#: templates/messages/list-header.php:2
msgid "New message"
msgstr "Nuovo messaggio"

#: templates/widgets/friends.tpl.php:46
msgid "View All"
msgstr "Mostra tutti"

#: templates/widgets/friends.tpl.php:52
#: templates/widgets/mutual-friends.tpl.php:43
msgid "No friends"
msgstr "Nessun amico"

#: templates/widgets/friendsbirthday.tpl.php:21
#: templates/widgets/friendsbirthday.tpl.php:54
msgid "Birthday Today"
msgstr "Compleanno oggi"

#: templates/widgets/friendsbirthday.tpl.php:39
msgid "Say Happy Birthday!"
msgstr ""

#: templates/widgets/friendsbirthday.tpl.php:55
msgid "No friend's birthdays today."
msgstr "Nessun compleanno dei tuoi amici oggi."

#: templates/widgets/friendsbirthday.tpl.php:67
#: templates/widgets/friendsbirthday.tpl.php:116
msgid "Upcoming Birthday"
msgstr "Prossimo compleanno"

#: templates/widgets/friendsbirthday.tpl.php:100
#, php-format
msgid "Birthday tomorrow."
msgid_plural "Birthday in %s days.</span>"
msgstr[0] "Il compleanno è oggi."
msgstr[1] "Il compleanno sarà tra %s giorni. </span>"

#: templates/widgets/friendsbirthday.tpl.php:117
msgid "No upcoming birthdays."
msgstr "Nessun compleanno imminente."

#. Plugin Name of the plugin/theme
msgid "PeepSo Core: Friends"
msgstr "PeepSo Core: Amici"

#. Plugin URI of the plugin/theme
#. Author URI of the plugin/theme
msgid "https://peepso.com"
msgstr "https://peepso.com"

#. Description of the plugin/theme
msgid "Friend connections and \"friends\" privacy level"
msgstr "Connessioni degli amici e livello di privacy degli amici"

#. Author of the plugin/theme
msgid "PeepSo"
msgstr "PeepSo"

