	<div class="tab-login">

		<ul class="tabs">
			<li class="tab-link" data-tab="tab-1">Register</li>
			<li class="tab-link current" data-tab="tab-2">Login</li>
		</ul>

		<div id="tab-1" class="tab-content">
			<div class="ps-register__form">
				<?php echo do_shortcode('[peepso_register]'); ?>
			</div>
		</div>

		<div id="tab-2" class="tab-content current">

			<?php PeepSoTemplate::exec_template('general','login');
			// echo do_shortcode('[peepso_login]'); ?>

		</div>
	
	</div>

<script>
	jQuery(document).ready(function($){

		$("ul.tabs li").click(function(){
			var tab_id = $(this).attr("data-tab");

			$("ul.tabs li").removeClass("current");
			$(".tab-content").removeClass("current");

			$(this).addClass("current");
			$("#"+tab_id).addClass("current");
		});

	  	$('.ps-form__field input#username').attr('placeholder', 'Enter your first and last name');
	  	$('.ps-form__field input#email').attr('placeholder', 'Enter your email address');
	  	$('.ps-form__field input#password').attr('placeholder', 'Enter your desired password');
	  	$('.ps-form__field input#password2').attr('placeholder', 'Re-type your password');
		$('.ps-form__field button.ps-btn.ps-btn-primary').text('Create your account');

	});
</script>