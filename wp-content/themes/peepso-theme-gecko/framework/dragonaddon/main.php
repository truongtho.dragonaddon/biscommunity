<?php

add_action('init', 'myStartSession', 1);
function myStartSession() {
	
    if(!session_id()) {
        session_start();
    } 

    if (isset($_GET['m']))
    	$_SESSION['module'] = $_GET['m'];  

	if (isset($_GET['m']) && $_GET['m'] == 'all')
    	unset($_SESSION['module']);
}

add_action('wp_enqueue_scripts', 'dragonaddon_add_scripts');
function dragonaddon_add_scripts() { 
    wp_enqueue_script('owl-carousel-script', get_template_directory_uri() .'/framework/dragonaddon/js/owl.carousel.min.js', array ('jquery'), '20200909', false);
    wp_enqueue_style('owl-carousel-style', get_template_directory_uri() .'/framework/dragonaddon/css/owl.carousel.min.css', '20200909', false);
	wp_enqueue_style('owl-carousel-default-style', get_template_directory_uri() .'/framework/dragonaddon/css/owl.theme.default.min.css', '20200909', false);

	wp_enqueue_script('dragonaddon-script', get_template_directory_uri() .'/framework/dragonaddon/js/dragonaddon_script.js', array ('jquery'), time(), false);
    wp_enqueue_style('dragonaddon-style', get_template_directory_uri() .'/framework/dragonaddon/css/dragonaddon_style.css', time(), false);
}


add_action('template_redirect', 'dragonaddon_hook_template_redirect', 5);
function dragonaddon_hook_template_redirect(){
	global $post, $pagenow;
	$currentID = $post->ID;
	$front = get_option( 'page_on_front' );
    if ( $front == $currentID && is_user_logged_in()) {
		$link = get_permalink(5);
        wp_redirect( $link );
		exit();
    }
}

add_shortcode('dragonaddon_add_carousel_products','dragonaddon_register_shortcode_carousel');
function dragonaddon_register_shortcode_carousel($attr){

	$html = '';
	$html .='<div class="owl-carousel owl-theme owl-products">

	    <div class="item">
	    	<div class="image">
	    		<img src="http://social.bostonindustrialsolutions.com/wp-content/uploads/2020/09/90mm-double-sided-ceramic-rings-for-pad-printer-Boston-Industrial-Solutions.jpg" alt="">
	    	</div>
	    	<div class="description">
	    		<h5>First slide label</h5>
				<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
				<a class="btn btn-warning" href="#">Shop Now</a>
	    	</div>
	    </div>

	    <div class="item">
	    	<div class="image">
	    		<img src="http://social.bostonindustrialsolutions.com/wp-content/uploads/2020/09/Natron-ST-Series-inks-pad-printing-inks-for-soft-touch-Boston-Industrial-Solutions-1-1024x1024-1.jpg" alt="">
	    	</div>
	    	<div class="description">
	    		<h5>First slide label</h5>
				<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
				<a class="btn btn-warning" href="#">Shop Now</a>
	    	</div>
	    </div>

	    <div class="item">
	    	<div class="image">
	    		<img src="http://social.bostonindustrialsolutions.com/wp-content/uploads/2020/09/silicone-ink-for-printing-silicone-rubber-Natron-SE-Series-Boston-Industrial-Solutions.jpg" alt="">
	    	</div>
	    	<div class="description">
	    		<h5>First slide label</h5>
				<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
				<a class="btn btn-warning" href="#">Shop Now</a>
	    	</div>
	    </div>

	    <div class="item">
	    	<div class="image">
	    		<img src="http://social.bostonindustrialsolutions.com/wp-content/uploads/2020/09/Siliocne-inks-for-screen-printing-textile-Boston-Industrial-Solutions.jpg" alt="">
	    	</div>
	    	<div class="description">
	    		<h5>First slide label</h5>
				<p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
				<a class="btn btn-warning" href="#">Shop Now</a>
	    	</div>
	    </div>

	</div>

	<script>
		jQuery(document).ready(function($){
			$(".owl-carousel").owlCarousel({
			    loop: true,
			    nav: false,
			    dots: true,
			    autoplay: true,
			    responsive:{
			        0:{
			            items:1
			        },
			        600:{
			            items:1
			        },
			        1000:{
			            items:1
			        }
			    }
			});
		});
	</script>';
	
	return $html;
}

add_shortcode('dragonaddon_add_tabs','dragonaddon_register_shortcode_tabs');
function dragonaddon_register_shortcode_tabs(){
    global $post;
	$user = PeepSoUser::get_instance()->get_profile_accessibility();

	if( $user ){

		$html .='<section id="component" role="article" class="ps-clearfix">';
            $html .='<div class="ps-page-register cRegister">';
                $html .='<h4 class="ps-page-title">'.__('You are already registered and logged in', 'peepso-core').'</h4>';
            $html .='</div>';
            $html .= sprintf(__('Visit <a href="%s">community</a> or <a href="%s">your profile</a>', 'peepso-core'), PeepSo::get_page('activity'), PeepSoUser::get_instance()->get_profileurl());
        $html .='</section>';

        return $html;

	}

	$html = '';

	$html .= get_template_part('framework/dragonaddon/templates/interact');
    
    $code = get_field('home_footer_shortcode', $post->ID);
	$html .= do_shortcode($code);

	return $html;
}


add_shortcode('dragonaddon_add_nemu_login','dragonaddon_add_nemu_login');
function dragonaddon_add_nemu_login(){
    global $post;
	$user = PeepSoUser::get_instance()->get_profile_accessibility();

 	$login = is_user_logged_in();

 	if ($login) {
 		echo 'login';
 		//echo do_shortcode( '[elementor-template id="850"]' );
 	} else {
 		echo 'not login';
 		//echo do_shortcode( '[elementor-template id="858"]' );
 	}	
}
?>
